
import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage
} from 'react-native';
import {persistStore, autoRehydrate} from 'redux-persist';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { createLogger } from 'redux-logger';
import reducer from './application/reducers';
import AppContainer from './application/containers/AppContainer';

const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__});
var reduxMidleware;

if (__DEV__) {
  alert("dev mode");
  const freeze = require('redux-freeze');
  reduxMidleware = applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
    freeze
  );
} else {
  reduxMidleware = applyMiddleware(
    thunkMiddleware
  );
}

function configureStore(initialState){
  const enhancer = compose(
    reduxMidleware,
    autoRehydrate()
  );
  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});

const App = () => (
  <Provider store={store}>
    <AppContainer store={store}/>
  </Provider>
);

AppRegistry.registerComponent('InteractMaVoix', () => App);
