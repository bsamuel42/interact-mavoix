'use strict';
import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions
} from 'react-native';

let Window = Dimensions.get('window');

var styles = StyleSheet.create({
  navigator: {flex: 1},
  container: {
		flex                : 1,
    width               : Window.width,
		backgroundColor     : '#F5FCFF',
	},
  maVoixHeader: {
    flexDirection       : 'row'
	},
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
});



module.exports = styles;
