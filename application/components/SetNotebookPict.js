import React, { Component } from 'react';

import {
  Text,
  View,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';


import styles from '../styles/styles';
import MaVoixHeader from './MaVoixHeader';
import MaVoixLibrary from './MaVoixLibrary';

const debug = true;

class SetNotebookPict extends Component {
  constructor(){
    super();
    this.state = {
      data: {},
    };
  }

  componentWillMount() {
    let arrTmp = [];
    this.setState({
      data: JSON.parse(JSON.stringify(this.props.notebook))
    });
  }

	render() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        "props: ",
        this.props,
        "state: ",
        this.state
      );
    }
  	return (
      <View>
        <MaVoixHeader
          {...this.props}
          page='SetNotebookPict'
          color={this.props.settings.color.header}
          height={Dimensions.get('window').height * 0.10}
          width={Dimensions.get('window').width}
        />
        <MaVoixLibrary
          page='Admin'
          notebookToSet={this.props.notebookToSet}
          navigator={this.props.navigator}
          elem_tab={this.state.elem_tab}
          color={this.props.settings.color.body}
          height={Dimensions.get('window').height * 0.90}/>
      </View>
		);
	}
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    notebook: state.notebook,
    actual_notebook: state.actual_notebook,
    elem_tab: state.elem_tab,
    fullPictLib: state.notebook[0]
  };
}

export default connect(mapStateToProps)(SetNotebookPict);
