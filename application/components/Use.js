import React, { Component } from 'react';

import {
  Text,
  View,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';


import styles from '../styles/styles';
import MaVoixHeader from './MaVoixHeader';
import MaVoixLibrary from './MaVoixLibrary';
import MaVoixSpArea from './MaVoixSpArea';

const debug = false;

class Use extends Component {
  constructor() {
    super();
    this.state = {
      elem_tab: {},
      sp_items: [],
    };
  }

  _navigate(page = 'Admin'){
    this.props.navigator.replace({
      name: page,
      props: this.props,
      store: this.props.store,
      passProps: {}
    })
  }

  componentWillMount() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }
    if ( this.props.notebook[this.props.actual_notebook] == undefined ||
      this.props.notebook[this.props.actual_notebook].lib_lst.length < 1) {
      alert("Veuillez crer un classeur contenant au moin une categorie");
      this._navigate('Admin');
    }
    this.setState({elem_tab: JSON.parse(JSON.stringify(this.props.elem_tab))});
  }

	render() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }
  	return (
      <View>
        <MaVoixHeader
          {...this.props}
          page='Use'
          color={this.props.settings.color.header}
          height={Dimensions.get('window').height * 0.10}
          width={Dimensions.get('window').width}
        />
        <MaVoixLibrary
          addSpLog={this.props.addSpLog}
          page='Use'
          navigator={this.props.navigator}
          elem_tab={this.state.elem_tab}
          color={this.props.settings.color.body}
          height={Dimensions.get('window').height * 0.90}/>
      </View>
		);
	}
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    actual_notebook: state.actual_notebook,
    notebook: state.notebook,
    elem_tab: state.elem_tab,
  };
}

export default connect(mapStateToProps)(Use);
