import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions';

import {
  View,
  Image,
  Button,
  TouchableHighlight,
  Alert,
  Text
} from 'react-native';

import styles from '../styles/styles';

const debug = false;

class MaVoixHeader extends Component {
  constructor(){
    super();
    this.onPress = this.onPress.bind(this);
  }

  _navigate(page){
    if (page === "Use" && this.props.actual_notebook == null  && !this.props.notebook.lib_lst[0]) {
      Alert.alert(
        'Classeur',
        'Il n\'y as aucun classeur sélectioner.'
      );
    } else {
      this.props.navigator.replace({
        name: page,
        props: this.props,
        store: this.props.store,
        passProps: {}
      })
    }
  }

  onPress() {
    if (this.props.page === "Admin") {
      this._navigate('Use');
    } else if (this.props.page === "SetNotebookPict") {
      this._navigate('Admin');
    } else if (this.props.page === "Use"){
      this._navigate('Admin');
    } else if (this.props.page === "Modal"){
      this.props.hide();
    }
  }

	render() {
    var pict = require('../public/ma_voix_501_198.png');
    if (this.props.page === "Admin") {
      console.log(this.props);
      return (
        <View style={[styles.maVoixHeader, { backgroundColor: this.props.color, height: this.props.height, justifyContent: 'space-between'}]}>
          <TouchableHighlight onPress={ this.onPress }>
            <Image source={pict} style={{height: this.props.height, width: this.props.height * 2.5}}/>
          </TouchableHighlight>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{fontSize: 25}}> Administration </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center', margin: 10}}>
            <Button
              onPress={ () => {
                  alert("Veillez Redemarez l'application pour prendre en compte le Reset");
                  this.props.resetApp()}}
              title="Réinitialiser"
              color="red"
              accessibilityLabel="Réinitialiser l'application"
            />
          </View>
        </View>
  		);
    } else if (this.props.page === "SetNotebookPict") {
      return (
        <View style={[styles.maVoixHeader, { backgroundColor: this.props.color, height: this.props.height}]}>
          <TouchableHighlight onPress={ this.onPress }>
            <Image source={pict} style={{height: this.props.height, width: this.props.height * 2.5}}/>
          </TouchableHighlight>
          <View style={{marginLeft: 10}}>
            <Text style={{fontSize: 25}}> Paramétrage des images</Text>
          </View>
        </View>
  		);
    } else {
      return (
        <View style={styles.maVoixHeader, { backgroundColor: this.props.color, width: this.props.height * 2.5, height: this.props.height}}>
          <TouchableHighlight onPress={ this.onPress }>
            <Image source={pict} style={{height: this.props.height, width: this.props.height * 2.5}}/>
          </TouchableHighlight>
        </View>
      );
    }
	}
}

function mapStateToProps(state) {
  if (__DEV__ && debug) {
    console.log(
      this.constructor.name,
      arguments.callee.toString().match(/function ([^\(]+)/)[1],
      state
    );
  }
  return {
    notebook: state.notebook,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MaVoixHeader);
