import React, { Component } from 'react';

import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Modal
} from 'react-native';
import { connect } from 'react-redux';

import styles from '../styles/styles';
import PhotoGrid from 'react-native-photo-grid';
import AddPictForm from './AddPictForm';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const debug = false;

var middle = <View/>
var compte = 0;

class MaVoixSpArea extends Component {
  constructor() {
    super();
    this.state = {
      sp_items: [],
      binder: {},
      data: {},
      itemPos: null,
      indexCollection: 0,
      selectPictModalVisible: false,
      x: 0,
      selectedPict: undefined
    };
    this.showSelectPictModalVisible = this.showSelectPictModalVisible.bind(this);
    this.renderItem = this.renderItem.bind(this);
    middle = <View key={this.state.x}/>
  }

  componentWillMount() {
  }

  renderItem(item, itemSize) {
    itemId++;
    var img = null;
    var txt = null;
    var source = this.props.sources[item.source];
    if (item.source != null && source.uri) {
      img = <View
        style={{flexDirection: 'row'}}><Image
      resizeMode = "contain"
      style = {{margin: 10}}
      source = {{ uri: source.uri, width: item.style.width, height: item.style.height }}
      ></Image></View>
      if (this.props.settings.text.show) {
          txt = <View>
            <Text
              style = {[
                {
                  maxWidth: windowWidth / this.props.settings.speak_area_size,
                  flexDirection: 'row',
                  justifyContent: 'center'
                },
                this.props.settings.text.style
              ]}>
              { item.text }
            </Text>
          </View>
      }
    }
    var pos = itemId;
    return(
      <TouchableHighlight
        underlayColor='transparent'
        key = { itemId }
        style = {{ maxWidth: windowWidth / this.props.settings.speak_area_size, width: itemSize + 3}}
        onPress = { () => {
          this.onElemPress(item, pos);
        }}>
        <View style = {{flexDirection: 'column', justifyContent: 'center'}}>
          { img }
          { txt }
        </View>
      </TouchableHighlight>
    )
  }

  onElemPress(item, pos) {
    if (this.props.settings.selectMode == 0 || this.props.settings.selectMode == 2) {
      this.props.delPictFromSpArea(item, pos);
    }
  }

  showSelectPictModalVisible(){
    this.setState({selectPictModalVisible: !this.state.selectPictModalVisible});
  }

	render() {
    itemId = -1;
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        "props: ",
        this.props,
        /*"state: ",
        this.state*/
      );
    }

    var pict = this.props.data.map((item, index) => {
      return this.renderItem(item, windowWidth / this.props.settings.speak_area_size)
    })

    return (
      <View style={{
          backgroundColor: this.props.settings.color.speakAreaColor,
          minHeight: this.props.height,
          flexDirection: 'row'
        }}>
        { pict }
      </View>
		);
	}
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    lib_lst: state.lib_lst,
    //elem_tab: state.elem_tab,
    sources: state.sources,
    sp_area: state.sp_area
  };
}

export default connect(mapStateToProps)(MaVoixSpArea);
