import React, { Component } from 'react';

import {
  View,
  ScrollView,
  Text,
  Dimensions,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Modal
} from 'react-native';
import { connect } from 'react-redux';

import styles from '../styles/styles';
import PhotoGrid from 'react-native-photo-grid';
import AddPictForm from './AddPictForm';
import MaVoixSpArea from './MaVoixSpArea';
import MaVoixHeader from './MaVoixHeader';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const debug = false;


var middle = <View/>
var compte = 0;

class MaVoixLibrary extends Component {
  constructor() {
    super();

    this.state = {
      sp_log: {
        start_time: null,
        end_time: null,
        stages: []
      },
      elem_tab: {},
      sp_items: [],
      binder: {},
      data: {},
      itemPos: null,
      indexCollection: 0,
      selectPictModalVisible: false,
      x: 0,
      selectedPict: undefined
    };
    this.showSelectPictModalVisible = this.showSelectPictModalVisible.bind(this);
    this.addToSp = this.addToSp.bind(this);
    this.delFromSp = this.delFromSp.bind(this);
    this.renderHeader = this.renderHeader.bind(this);

    middle = <View key={this.state.x}/>
  }

  componentWillMount() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        "props: ",
        this.props,
        "state: ",
        this.state
      );
    }

    if ( this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet] == undefined ||
      this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet].lib_lst.length < 1) {
      alert("Veuillez créer au moin une categorie dans le classeur selectioner");
      console.log(this.props);
      this.props.navigator.replace({
        name: "Admin",
        props: this.props,
        store: this.props.store,
        passProps: {}
      })
    }


    if (this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet] && this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet].lib_lst[0]) {
      let data = this.props.elem_tab[this.props.lib_lst[this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet].lib_lst[0]].elem_tab].value;
      let arrTmp = [];
      arrTmp = data;

      this.setState({actual_tab: this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet].lib_lst[0], x: this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet].lib_lst[0]});
    }
  }

  changeTab(tabId) {
    if (this.state.actual_tab !== tabId) {
      this.setState({ actual_tab: tabId});
    }
  }

  addToSp(item, tabId, pos){
    if (this.state.sp_items.length < this.props.settings.speak_area_size) {
      var sp_log = JSON.parse(JSON.stringify(this.state.sp_log));
      let sp_items = [...this.state.sp_items, Object.assign({}, item, {from: { tabId: tabId, pos: pos}})];
      if (this.state.sp_items.length == 0) {
        sp_log.start_time = new Date().getTime();
      }
      item.display = false;
      sp_log.stages.push(sp_items);
      sp_log.end_time = new Date().getTime();
      this.setState({
        sp_log: sp_log,
        sp_items: sp_items,
        elem_tab: JSON.parse(JSON.stringify(this.state.elem_tab))
      });
    }
    console.log("addToSp sp_log: ", sp_log);
  }

  delFromSp(item, pos){
    //var sp_log = JSON.parse(JSON.stringify(this.state.sp_log));
    var new_sp_items = [];
    this.props.elem_tab[item.from.tabId].value[item.from.pos].display = true;
    this.state.sp_items.map((item, index) => {
      if (index !== pos) {
        new_sp_items.push(item);
      }
    });
    //sp_log.stages.push(new_sp_items);
    this.setState({
      //sp_log: sp_log,
      sp_items: new_sp_items,
      elem_tab: JSON.parse(JSON.stringify(this.state.elem_tab))
    });
    if (new_sp_items.length == 0) {
      this.props.addSpLog(JSON.parse(JSON.stringify(this.state.sp_log)));
      this.setState({
        sp_log: {
          start_time: null,
          end_time: null,
          stages: []
        }
      });
    }
  }

  renderHeader(){
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        "props: ",
        this.props,
        "state: ",
        this.state
      );
    }
    let arrData = [];
    let index = 0;
    let index3 = 0;

    this.props.notebook[(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet].lib_lst.map((x) => {
      if (!isNaN(x)) {
        this.state.indexCollection = 0;
        var tmp = null;

        if (x != this.state.actual_tab) {
            tmp = <TouchableHighlight
                key={x + index}
                underlayColor='transparent'
                style={{minWidth: 50, marginHorizontal: 3,  backgroundColor: '#FCF1A5'}}
                onPress={() => {this.changeTab(x); }}
              >
                <Text style={{minWidth: 50, fontSize: 25, color: '#84642E', padding: 2}}>
                  {this.props.lib_lst[x].text}
                </Text>
              </TouchableHighlight>
        } else {
          tmp = <View
            key={x + index}
            style={{minWidth: 50, marginHorizontal: 3,  backgroundColor: '#84642E'}}>
              <Text style={{minWidth: 50, fontSize: 25, color: '#FCF1A5', padding: 2}}>
                {this.props.lib_lst[x].text}
              </Text>
            </View>
        }
        arrData.push( tmp );
        index++;
      }
    });

    return (
      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
        <TouchableOpacity style={{paddingHorizontal: 1, backgroundColor: '#FCF1A5'}}>
          <Text style={{fontSize: 25, color: '#84642E'}}>{'<'}</Text>
        </TouchableOpacity>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ScrollView horizontal={true} style={{maxWidth: windowWidth * 0.9}}>
            <View style={{flexDirection: 'row', width: windowWidth}}>
              {arrData}
            </View>
          </ScrollView>
        </View>
        <TouchableOpacity style={{paddingHorizontal: 5, backgroundColor: '#FCF1A5'}}>
          <Text style={{fontSize: 25, color: '#84642E'}}>{'>'}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderItem(item, itemSize , that) {
    itemId++;
    var img = null;
    var txt = null;
    var source = this.props.sources[item.source];
    if (item.display && item.source != null && source.uri) {
      img = <View
        style={{flexDirection: 'row', justifyContent: 'center'}}><Image
      resizeMode = "contain"
      style = {{margin: 10}}
      source = {{ uri: source.uri, width: item.style.width, height: item.style.height }}
      ></Image></View>
      if (this.props.settings.text.show) {
          txt = <View>
            <Text
              style = {[
                {
                  maxWidth: windowWidth / this.props.lib_lst[this.state.actual_tab].setting.nb_colonne - 10,
                  flexDirection: 'row',
                  justifyContent: 'center'
                },
                this.props.settings.text.style
              ]}>
              { item.text }
            </Text>
          </View>
      }
    } else if (this.props.page == "Admin") {
      img = <View><Image
        resizeMode = "contain"
        style = {{margin: 10}}
        source = { require('../public/plus-sign.png') }
      ></Image></View>
    }
    var pos = itemId;
    return(
      <TouchableHighlight
        underlayColor='transparent'
        key = { itemId }
        style = {{ maxWidth: windowWidth / this.props.lib_lst[this.state.actual_tab].setting.nb_colonne, width: itemSize + 3, height: itemSize}}
        onPress = { () => {
          this.onElemPress(item, pos);
        }}>
        <View style = {{flexDirection: 'column', justifyContent: 'center'}}>
          { img }
          { txt }
        </View>
      </TouchableHighlight>
    )
  }

  onElemPress(item, pos) {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        item,
        pos,
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }
    if (this.props.settings.selectMode == 0 || this.props.settings.selectMode == 2) {
      if (this.props.page == "Admin") {
        this.setState({selectedPict: item, itemPos: pos});
        this.showSelectPictModalVisible();
      } else if (this.props.page == "Use") {
        //alert("must go to SP if less than " + this.props.settings.speak_area_size + " elem in SP");
        this.addToSp(item, this.state.actual_tab, pos);
      } else if (this.props.page == "Select") {
        alert("must Update lib elem");
      }
    }
  }

  showSelectPictModalVisible(){
    this.setState({selectPictModalVisible: !this.state.selectPictModalVisible});
  }

	render() {
    itemId = -1;
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        "props: ",
        this.props,
        "state: ",
        this.state
      );
    }

    var selectPictModal = null;
    var notebookHeight = this.props.height * 0.75;
    var speakArea = <View style={{backgroundColor: this.props.settings.color.speakAreaColor, minHeight: this.props.height * 0.25}}>
      <PhotoGrid
        data = { this.state.sp_items }
        itemsPerRow = { this.props.settings.peak_area_size }
        itemMargin = { 10 }
        renderItem = { this.renderSPItem }
      />
    </View>

    if (this.props.page === "Admin") {
      selectPictModal = <View style={{marginTop: 22}}>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.selectPictModalVisible}
          onRequestClose={() => {this.showSelectPictModalVisible()}}
          >
         <View>
          <MaVoixHeader
            {...this.props}
            page='Modal'
            color={this.props.settings.color.header}
            height={Dimensions.get('window').height * 0.10}
            width={Dimensions.get('window').width}
            hide={ this.showSelectPictModalVisible }
          />
          <AddPictForm
            color={this.props.settings.color.settingBody}
            height={Dimensions.get('window').height * 0.90}
            setingNotebook={this.props.setingNotebook}
            updateElemTab={this.props.updateElemTab}
            notebook={(this.props.page != "Admin" ) ? this.props.actual_notebook : this.props.notebookToSet}
            item={ this.state.selectedPict }
            itemPos={this.state.itemPos}
            actual_tab={this.state.actual_tab}
            hide={ this.showSelectPictModalVisible }/>
         </View>
        </Modal>
      </View>
    }
    if (this.props.lib_lst[this.state.actual_tab]) {
      return (
        <View style={styles.maVoixPictTable, {maxHeight: this.props.height}}>
          <View style = {{backgroundColor: this.props.settings.color.body, height: notebookHeight}}>
            <PhotoGrid
              key = {this.state.x + 1}
              data = { this.props.elem_tab[this.props.lib_lst[this.state.actual_tab].elem_tab].value }
              itemsPerRow = { this.props.lib_lst[this.state.actual_tab].setting.nb_colonne }
              itemMargin = { 10 }
              renderHeader = { this.renderHeader }
              renderItem = { data => this.renderItem(data) }
            />
          </View>
          <MaVoixSpArea
            delPictFromSpArea={this.delFromSp}
            elem_tab={this.props.elem_tab}
            data={this.state.sp_items}
            height={this.props.height * 0.25}/>
          {selectPictModal}
        </View>
  		);
    } else {
      return (null);
    }
	}
}



function mapStateToProps(state) {
  return {
    settings: state.settings,
    notebook: state.notebook,
    actual_notebook: state.actual_notebook,
    lib_lst: state.lib_lst,
    elem_tab: JSON.parse(JSON.stringify(state.elem_tab)),
    sources: state.sources
  };
}

export default connect(mapStateToProps)(MaVoixLibrary);
