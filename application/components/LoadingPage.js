import React, { Component } from 'react';

import {
  View,
  Dimensions,
  Image,
  Text
} from 'react-native';
import { connect } from 'react-redux';


import styles from '../styles/styles';
import MaVoixHeader from './MaVoixHeader';
import MaVoixLibrary from './MaVoixLibrary';
import MaVoixSpArea from './MaVoixSpArea';

const debug = false;

class LoadingPage extends Component {
  constructor() {
    super();
    this.state = {
      elem_tab: {},
      sp_items: [],
    };
  }


  componentWillMount() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }
    //this.setState({elem_tab: JSON.parse(JSON.stringify(this.props.elem_tab))});
  }

	render() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }
    var loading_text = ''
    if (this.props.first) {
      loading_text = "Please wait we are starting the app"
    } else {
      loading_text = "We are loading all data"
    }
  	return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <Image source={require('../public/ma_voix_1002_396.png')} />
        <Text style={{
          flex: 1,
          fontSize: 25
        }}>{loading_text}</Text>
      </View>
		);
	}
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps)(LoadingPage);
