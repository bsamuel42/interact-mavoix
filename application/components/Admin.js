import React, { Component } from 'react';

import {
  Text,
  View,
  Dimensions,
  ScrollView,
  TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';

import styles from '../styles/styles';
import MaVoixHeader from './MaVoixHeader';
import MaVoixSettings from './MaVoixSettings';

class Admin extends Component {
  constructor(){
    super();
    this.state = {
      first: false
    };
  }

  componentWillMount(){
    this.setState({first: this.props.first})
  }

  tutorial(){
    if (this.state.first) {
        alert("Veuillez créer la premiére cathégorie du classeur");
        this.state.first = false;
    }
  }

	render() {
    this.tutorial();
    return (
      <View style={{flex: 1}}>
        <MaVoixHeader
          {...this.props}
          page='Admin'
          color={this.props.settings.color.header}
          height={Dimensions.get('window').height*0.10}
          width={Dimensions.get('window').width}
        />
        <MaVoixSettings
          addNotebook={this.props.addNotebook}
          addTab={this.props.addTab}
          height={Dimensions.get('window').height*0.90}
          navigator={this.props.navigator}
          color={this.props.settings.color.settingBody}
        />
    </View>
    );
	}
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    notebook: state.notebook,
    actual_notebook: state.actual_notebook
  };
}
export default connect(mapStateToProps)(Admin);
