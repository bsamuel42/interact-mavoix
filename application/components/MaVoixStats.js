import React, { Component } from 'react';

import {
  View,
  ScrollView,
  FlatList,
  Text,
  Dimensions,
  Image,
  SectionList
} from 'react-native';

import styles from '../styles/styles';
import PhotoGrid from 'react-native-photo-grid';

const windowWidth = Dimensions.get('window').width;
const debug = true;

class MaVoixStats extends Component {
  constructor() {
    super();
    this.state = {
    };
    this.renderItem = this.renderItem.bind(this);
  }

  componentWillMount() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }

  }

  renderItem(item, itemSize, that) {
    console.log(item);
    var finalItem = [];
    var img = null;
    var txt = null;
    item.item.map((logItem, index) => {
      var source = this.props.sources[logItem.source];
      img = (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center'
          }}
        >
          <Image
            resizeMode = "contain"
            style = {{margin: 10}}
            source = {{ uri: source.uri, width: logItem.style.width, height: logItem.style.height }}
            />
        </View>
      );

      txt = (<View>
        <Text
          style = {[
            {
              flexDirection: 'row',
              justifyContent: 'center'
            },
            this.props.settings.text.style
          ]}>
          { logItem.text }
        </Text>
      </View>);
      console.log("key", item.index + '-' + index);
      finalItem.push(<View key={item.index + '-' + index} style = {{flexDirection: 'column', justifyContent: 'center'}}>
        { img }
        { txt }
      </View>)
    })
    return(
      <View style = {{flexDirection: 'row', justifyContent: 'center'}}>
        {finalItem}
      </View>
    );
  }

  renderHeader(section){
    console.log("key", section.section.key);
    return (<Text key={section.section.key} style={styles.sectionHeader}>{section.section.title}</Text>);
  }

	render() {
    itemId = -1;
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        "props: ",
        this.props,
        "state: ",
        this.state
      );
    }

    return (
      <SectionList
        sections={this.props.splog}
        renderItem={this.renderItem}
        renderSectionHeader={this.renderHeader}
      />
		);
	}
}

export default MaVoixStats;
