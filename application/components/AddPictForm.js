import React, { Component } from 'react';
import { connect } from 'react-redux';
import RNFetchBlob from 'react-native-fetch-blob'
import { bindActionCreators } from 'redux';

import {
  Platform,
  View,
  ScrollView,
  Image,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Switch,
  Button
} from 'react-native';

import styles from '../styles/styles';
import PhotoGrid from 'react-native-photo-grid';
import { ActionCreators } from '../actions';
var ImagePicker = require('react-native-image-picker');

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const debug = false;
const defaultPict = require('../public/plus-sign.png');

class AddPictForm extends Component {
  constructor(){
    super();
    this.state = {
      newSource: {id: null, uri: defaultPict},
      shareItem: {},
      dispPictLib: true,
      size: '',
      item: {}
    };
    this.newPict = this.newPict.bind(this);
  }

  componentWillMount() {
    let data = this.props.elem_tab[this.props.lib_lst[this.props.fullPictLib.start_tab].elem_tab].value;
    let arrTmp = [];
    arrTmp = data;

    var item = Object.assign({}, this.props.item);

    this.setState({
      shareItem: this.props.item,
      data: this.props.fullPictLib.elem_tab,
      actual_tab: this.props.fullPictLib.start_tab,
      x: this.props.fullPictLib.start_tab,
      item: item,
      size: item.style.width.toString()
    });
  }

  newPict(){
    var options = {
      title: 'Select Pictures',
      mediaType: 'photo',
      noData: true,
    };
    var newSource = ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let item = this.state.item
        let source = {
          id: this.props.sources.lastId + 1,
          uri: response.path
        };
        var tmpUri = Platform.OS === 'android' ? 'file://' + source.uri  : '' + source.uri;

        try {
          Object.keys(this.props.sources).map((id, index) => {
            if (!isNaN(id) && this.props.sources[id] && !tmpUri.localeCompare(this.props.sources[id].uri)) {
              source = this.props.sources[id];
              item.source = id;
              console.log(source);
            }
          });
        } finally {
          if (source.id == this.props.sources.lastId + 1) {
            let dirs = RNFetchBlob.fs.dirs;
            source.uri = dirs.PictureDir + '/InteractMaVoix/' + source.id + '.png';
            source.uri = Platform.OS === 'android' ? 'file://' + source.uri  : '' + source.uri;
            RNFetchBlob.fs.cp(response.path, dirs.PictureDir + '/InteractMaVoix/' + source.id + '.png')
              .then(() => {
                console.log(source, item);
                this.setState({newSource: source, item: item});
                RNFetchBlob.fs.scanFile([ { path : dirs.PictureDir + '/InteractMaVoix/' + source.id + '.png'} ], 'image/png');
                this.props.addSources(source.id, source);
              });
            this.state.item.source = this.props.sources.lastId + 1;
          } else {
            this.setState({item: item});
          }

        }
        return source;
      }
    });
    this.setState({newSource: newSource});
  }

  render() {
    var source = null;
    if (this.state.item.source && this.props.sources[this.state.item.source])
    {
      source = {
        uri: this.props.sources[this.state.item.source].uri,
        width: this.state.item.style.width,
        height: this.state.item.style.height
      };
    } else if (this.state.item.source && this.state.newSource.uri) {
      source = {
        uri: this.state.newSource.uri,
        width: this.state.item.style.width,
        height: this.state.item.style.height
      };
    } else {
      source = defaultPict;
    }


    var selectedPict = (
      <TouchableOpacity onPress={this.newPict}>
        <Image
        resizeMode = "contain"
        style = {{margin: 10}}
        source = {source}
        ></Image>
      </TouchableOpacity>
    );
    return (
      <ScrollView style={{height: this.props.height, backgroundColor: this.props.color}}>
        <View style={{flexDirection: 'row', margin: 10, justifyContent: 'space-between',}}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text>Dimention de l'image </Text>
              <TextInput
               keyboardType = 'numeric'
               onChangeText = {(value) => {
                 if (value) {
                   this.setState({size: value});
                 } else {
                   this.setState({size: '0'});
                 }
               }}
               onSubmitEditing = {(event) => {
                 item = this.state.item;
                 item.style.width = parseInt(event.nativeEvent.text);
                 item.style.height = parseInt(event.nativeEvent.text);
                 this.setState({item: item});
               }}
               value = {this.state.size}
               maxLength = {4}
              />
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text>Text associer as l'image </Text>
              <TextInput
               keyboardType = 'default'
               onChangeText = {(value) => {
                 this.state.item.text = value.replace(new RegExp('  ', 'g'), ' ');
                 this.setState({item: this.state.item});
               }}
               style = {{width: 200}}
               placeholder = "Veuiller entre le text qui sera afficher"
               value = {this.state.item.text}
              />
            </View>
          </View>
          <View>
            {selectedPict}
          </View>
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'flex-start',
          marginTop: 10
        }}>
          <Button
            onPress={() => { this.validateItem(); }}
            title="Valider"
            color="green"
            accessibilityLabel="Valider les paramétre généraux"
          />
        </View>
      </ScrollView>
    );
	}

  renderHeader =  () =>  {
    let arrData = [];
    let index = 0;
    let index3 = 0;

    for (let x in this.props.lib_lst) {
      if (!isNaN(x)) {
        this.state.indexCollection = 0;
        arrData.push(
          <TouchableOpacity
            key={x + index}
            style={{minWidth: 50, marginHorizontal: 3,  backgroundColor: (x.localeCompare(this.state.actual_tab)) ? '#FCF1A5': '#84642E'}}
            onPress={() => { this.setState({ actual_tab: x});}}
          >
            <Text style={{minWidth: 50, fontSize: 25, color: (x.localeCompare(this.state.actual_tab)) ? '#84642E' : '#FCF1A5', padding: 2}}>
              {this.props.lib_lst[x].text}
            </Text>
          </TouchableOpacity>
        );
        index++;
      }
    }

    return (
      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
        <TouchableOpacity style={{paddingHorizontal: 1, backgroundColor: '#FCF1A5'}}>
          <Text style={{fontSize: 25, color: '#84642E'}}>{'<'}</Text>
        </TouchableOpacity>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ScrollView horizontal={true} style={{maxWidth: windowWidth * 0.9}}>
            <View style={{flexDirection: 'row', width: windowWidth}}>
              {arrData}
            </View>
          </ScrollView>
        </View>
        <TouchableOpacity style={{paddingHorizontal: 5, backgroundColor: '#FCF1A5'}}>
          <Text style={{fontSize: 25, color: '#84642E'}}>{'>'}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderItem(item, itemSize , that) {
    itemId++;
    var img = null;
    var txt = null;
    var source = this.props.sources[item.source];
    if (item.source !== null && source.uri) {
      img = <View
        style={{flexDirection: 'row', justifyContent: 'center'}}><Image
      resizeMode = "contain"
      style = {{margin: 10}}
      source = {{ uri: source.uri, width: item.style.width, height: item.style.height }}
      ></Image></View>
    }
    return(
      <TouchableOpacity
        key = { itemId }
        style = {{ maxWidth: windowWidth / this.props.lib_lst[this.state.actual_tab].setting.nb_colonne, width: itemSize + 3, height: itemSize}}
        onPress = { () => {
          this.onElemPress(item);
        }}>
        <View style = {{flexDirection: 'column', justifyContent: 'center'}}>
          { img }
          { txt }
        </View>
      </TouchableOpacity>
    )
  }

  onElemPress(selectedItem) {
    var item = this.state.item;
    item.source = selectedItem.source;
    this.setState({item: item, dispPictLib: false});
  }

  validateItem(){
    this.state.item.display = true;
    this.props.updateElemTab(this.props.actual_tab, this.state.item, this.props.itemPos);
    this.props.hide();
  }
}

function mapStateToProps(state) {
  return {
    fullPictLib: state.fullPictLib[0],
    settings: state.settings,
    lib_lst: state.lib_lst,
    elem_tab: state.elem_tab,
    sources: state.sources,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPictForm);
