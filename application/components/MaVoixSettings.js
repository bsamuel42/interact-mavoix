import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions';
import {
  View,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  Modal,
  Image,
  Text,
  TextInput,
  Picker,
  AsyncStorage,
  Button,
  Switch,
  Alert,
  FlatList
} from 'react-native';

//import { Switch } from 'react-native-switch';
import { ColorPicker,  toHsv, fromHsv} from 'react-native-color-picker'

import PhotoGrid from 'react-native-photo-grid';
import CheckBox from 'react-native-checkbox';
import styles from '../styles/styles';
import MaVoixStats from './MaVoixStats';

const debug = false;
const switchParam = {};
console.disableYellowBox = !debug;


class MaVoixSettings extends Component {
  constructor(){
    super();
    this.state = {
      settings: {},
      modalHeaderColor: false,
      allNotebook: {},
      newLib: {
        "name": null,
        "text": null,
        "style": {},
        "elem": '',
        "setting": {nb_line: "1", nb_colonne: "2"}
      },
      newNotebook: '',
      tmpNotebook: {
        "elem_tab": {},
        "lib_lst": {},
        start_tab: ''
      },
      dispGlobalSeting: false,
      dispNotebookSeting: false,
      dispStats: false,
      dispAllStage: false,
      setNotebook: null,
      setlib: null,
      setlib_nb_line: '',
      setlib_nb_colonne: ''
    };
    this.handleDispAllStage = this.handleDispAllStage.bind(this);
  }

  _navigate(page){
    this.props.navigator.replace({
      name: page,
      notebookToSet: this.state.setNotebook,
      props: this.props,
      passProps: {}
    })
  }

  componentWillMount() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }
    if ((this.props.notebook[this.props.settings.notebook] && this.props.notebook[this.props.settings.notebook].lib_lst[0])) {
      var lib = this.props.notebook[this.props.settings.notebook].lib_lst[0]
        this.setState({
          setlib: lib,
          setlib_nb_line: this.props.lib_lst[lib].setting.nb_line,
          setlib_nb_colonne: this.props.lib_lst[lib].setting.nb_colonne,
        })
    }
    this.setState({
      settings: JSON.parse(JSON.stringify(this.props.settings)),
      setNotebook: (this.props.notebook[this.props.settings.notebook]) ? this.props.settings.notebook : this.props.notebook.lastId,
      allNotebook: this.props.notebook,
      dispNotebookSeting: false
    });
  }

  handleDispAllStage(value){
    console.log(value, this.state.dispAllStage);
    this.setState({dispAllStage: !this.state.dispAllStage});
  }

  render() {
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        "props: ",
        this.props,
        "state: ",
        this.state
      );
    }

    var globalSetting = null;
    var notebookSetting = null;
    var stats = null;

    if (this.state.dispGlobalSeting) {
      globalSetting = this.renderGlobalSetting();
    }

    if (this.state.dispNotebookSeting) {
      notebookSetting = (
        <View>
          {this.renderNotebookSetting()}
        </View>
      );
    }

    if (this.state.dispStats) {
      let logData = [];

      Object.keys(this.props.sp_log).map((id, index) => {
        if (!isNaN(id)) {
          var item = this.props.sp_log[id];
          console.log(item, index);
          var newItem = {
            key: index,
            title: 'phrase faite le: ' + (new Date(item.start_time)).toUTCString(),
            data: []
          }
          if (this.state.dispAllStage) {
            newItem.data = item.stages;
          } else {
            if (item.stages.length > 0) {
              newItem.data.push(item.stages[item.stages.length -1]);
            }
          }
          logData.push(newItem);
        }
      })
      var dispAllStage = (
        <View style={{flexDirection: 'row'}}>
          <CheckBox
            label='Afficher toutes les étapes'
            checked={this.state.dispAllStage}
            containerStyle={{backgroundColor: 'transparent'}}
            underlayColor='transparent'
            onChange={this.handleDispAllStage}
          />
        </View>
      );
      stats = (
        <ScrollView>
          {dispAllStage}
          <MaVoixStats splog={logData} sources={this.props.sources} dispAllStage={this.state.dispAllStage} settings={this.props.settings}/>
        </ScrollView>
      );
    }

    return (
      <View style={{height: this.props.height, backgroundColor: this.props.color}}>
        <View style={{flexDirection: 'row', margin: 10}}>
          <Switch
            {...switchParam}
            onValueChange={(value) => {
              this.setState({dispStats: false, dispGlobalSeting: value, dispNotebookSeting: false});
            }}
            value={this.state.dispGlobalSeting}
          />
          <Text style={{marginLeft: 10}} >Paramétre global</Text>
        </View>
        {globalSetting}

        <View style={{flexDirection: 'row', margin: 10}}>
          <Switch
            {...switchParam}
            onValueChange={(value) => {
              this.setState({dispStats: false, dispGlobalSeting: false, dispNotebookSeting: value});
            }}
            value={this.state.dispNotebookSeting}
          />
          <Text style={{marginLeft: 10}}>Paramétre des classeurs</Text>
        </View>
        {notebookSetting}

        <View style={{flexDirection: 'row', margin: 10}}>
          <Switch
            {...switchParam}
            onValueChange={(value) => {
              this.setState({dispStats: value, dispGlobalSeting: false, dispNotebookSeting: false});
            }}
            value={this.state.dispStats}
          />
          <Text style={{marginLeft: 10}}>Phrase construite: </Text>
        </View>
        {stats}
      </View>
    );
  }

  renderGlobalSetting() {
    if (this.state.dispGlobalSeting) {
      if (__DEV__ && debug) {
        console.log(
          this.constructor.name,
          arguments.callee.toString().match(/function ([^\(]+)/)[1],
          /*"props: ",
          this.props,
          "state: ",
          this.state*/
        );
      }

      var setPictSelect = (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          marginTop: 10
        }}>
          <Text style={{ margin: 10}}>Selection des images</Text>
          <Picker
            style={{ flex: 1}}
            selectedValue={this.state.settings.selectMode}
            onValueChange={(value) => {
              var tmp = this.state.settings
              tmp.selectMode = value;
              this.setState({settings: tmp});
            }}>
              <Picker.Item label="Click" value="0" />
              <Picker.Item label="Drag and Drop" value="1" />
              <Picker.Item label="Click et Drag and Drop" value="2" />
          </Picker>
        </View>
      );

      var setSound = (
        <View style={{flexDirection: 'row'}}>
          <Switch
            {...switchParam}
            onValueChange={(value) => {
              var tmp = this.state.settings
              tmp.sound = value;
              this.setState({settings: tmp});
            }}
            value={this.state.settings.sound} />
          <Text>Sond</Text>
        </View>
      );

      var setText = (
        <View style={{flexDirection: 'row'}}>
          <CheckBox
            label='Afficher les textes'
            checked={this.state.settings.text.show}
            containerStyle={{backgroundColor: 'transparent'}}
            underlayColor='transparent'
            onChange={(value) => {
              var tmp = this.state.settings
              tmp.text = Object.assign({}, tmp.text, {"show": !tmp.text.show});
              this.setState({settings: tmp});
            }}
          />
        </View>
      );

      var spAreaSize = (
        <View style={{flexDirection: 'row'}}>
          <Text>Speak Area Size</Text>
          <TextInput
            keyboardType = 'numeric'
            onChangeText = {(value) => {
              var tmp = this.state.settings
              tmp.speak_area_size = value;
              this.setState({settings: tmp});
            }}
            value = {this.state.settings.speak_area_size}
            maxLength = {3}
          />
        </View>
      );

      var setSorting = (
        <View>
          <Text>Rangement des images</Text>
          <Picker
            selectedValue={this.state.settings.sorting}
            onValueChange={(value) => {
              var tmp = this.state.settings
              tmp.sorting = value;
              this.setState({settings: tmp});
            }}>
              <Picker.Item label="Désactiver" value="0" />
              <Picker.Item label="Manuel" value="1" />
              <Picker.Item label="Aléatoire" value="2" />
          </Picker>
        </View>
      );

      var setNavigation = (
        <View>
          <Text>Navigation</Text>
          <Picker
            selectedValue={this.state.settings.tabnav}
            onValueChange={(value) => {
              var tmp = this.state.settings
              tmp.tabnav = value;
              this.setState({settings: tmp});
            }}>
              <Picker.Item label="Click" value="0" />
              <Picker.Item label="Scroll" value="1" />
              <Picker.Item label="Click et Scroll" value="2" />
          </Picker>
        </View>
      );

      var validateGlobalSettings = (
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'flex-start',
          marginTop: 10
        }}>
          <Button
            onPress={() => { this.updateSettings(); }}
            title="Valider"
            color="green"
            accessibilityLabel="Valider les paramétre généraux"
          />
        </View>
      );

      let classeur_item = []
      Object.keys(this.props.notebook).map((id, index) => {
        if (!isNaN(id)) {
          classeur_item.push(<Picker.Item key={index} label={this.props.notebook[id].name} value={id} />);
        }
      });
      if (classeur_item.length == 0) {
        classeur_item.push(<Picker.Item key={classeur_item.length} label="Créer un classeur s'il vous plais" value={null} />);
      }
      var selectUseNotebook = (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          marginTop: 10
        }}>
          <Text
            style={{
              flex: 0.5,
              marginTop: 10
          }}>
            Classeur afficher:
          </Text>
          <Picker
            style={{
              flex: 4
            }}
            selectedValue={this.state.settings.notebook}
            onValueChange={(value) => {
              var tmp = this.state.settings
              tmp.notebook = value;
              this.setState({settings: tmp});
            }}>
            { classeur_item }
          </Picker>
        </View>
      )
      /*
        as suprimer pour afficher
      */
      setPictSelect = null;
      setSound = null;
      setSorting = null;
      setNavigation = null;
      /*
      */

      return (
        <View  style={{marginLeft: 10, marginRiht: 10, height: this.props.height / 3}}>
          {selectUseNotebook}
          {setPictSelect}
          {setSound}
          {setText}
          {spAreaSize}
          {setSorting}
          {setNavigation}
          {validateGlobalSettings}
        </View>
      );
    } else {
      return (null);
    }
  }

  renderNotebookSetting() {
    if (this.state.dispNotebookSeting) {
      if (__DEV__ && debug) {
        console.log(
          this.constructor.name,
          arguments.callee.toString().match(/function ([^\(]+)/)[1],
          /*"props: ",
          this.props,
          "state: ",
          this.state*/
        );
      }

      let lib_item = [];
      var lib_picker = null;
      var newNotebookForm = null;
      var supressNotebook = null;
      var setPict = null;
      let classeur_item = []

      Object.keys(this.props.notebook).map((id, index) => {
        if (!isNaN(id)) {
          classeur_item.push(<Picker.Item key={index} label={"Paramétrer le classeur: " + this.props.notebook[id].name} value={id} />);
        }
      });
      classeur_item.push(<Picker.Item key={classeur_item.length} label="Créer un classeur" value={null} />);

      if (this.state.setNotebook !== null && this.props.notebook[this.state.setNotebook]) {
        var lib_lst = this.props.notebook[this.state.setNotebook].lib_lst;
        index = 0;
        for(var key in lib_lst) {
          if(this.props.lib_lst.hasOwnProperty(lib_lst[key])) {
            var lib = lib_lst[key];
            lib_item.push( <Picker.Item key={index} label={"Paramétrer l'onglet: " + this.props.lib_lst[lib_lst[key]].text} value={lib_lst[key]} /> );
            index++;
          }
        }
        lib_item.push(<Picker.Item key='index' label="Créer un onglet" value={null} />);

        lib_picker = (
          <View>
            <Picker
              selectedValue={this.state.setlib}
              onValueChange={(value) => {
                console.log(value);
                if (this.state.setNotebook !== null && this.props.lib_lst.hasOwnProperty(value)) {
                  this.setState({
                    setlib: value,
                    setlib_nb_line: this.props.lib_lst[value].setting.nb_line,
                    setlib_nb_colonne: this.props.lib_lst[value].setting.nb_colonne
                  });
                } else {
                  this.setState({
                    setlib: value,
                    setlib_nb_line: '',
                    setlib_nb_colonne: ''
                  });
                }
              }}>
              { lib_item }
            </Picker>
          </View>
        );
        supressNotebook = (
          <View>
            <Button
              onPress={() => {
                this.props.delNotebook(this.state.setNotebook);
                this.setState({setNotebook: null, dispNotebookSeting: false});
              }}
              title="Suprimer"
              color="red"
            />
          </View>
        );
        setPict = (
          <View>
            <Button
              onPress={() => {
                this._navigate('SetNotebookPict');
              }}
              title="Paramétrer les images"
            />
          </View>
        );
      } else {
        newNotebookForm = (
          <View>
            <View>
              <Text style={{fontWeight: 'bold', fontSize: 30, textAlign:'center'}}>Creation d'un nouveau Classeur</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text>Nom: </Text>
              <TextInput
               keyboardType = 'default'
               onChangeText = {(value) => {
                 this.state.newNotebook = value.replace(new RegExp('  ', 'g'), ' ');
                 this.setState({newNotebook: value});
               }}
               onSubmitEditing = {(event) => {
                 try {
                   let tmp_classeur = JSON.parse(JSON.stringify(this.state.allNotebook));
                   tmp_classeur[this.props.notebook.lastId + 1] = {
                     "id": this.props.notebook.lastId + 1,
                     "name": event.nativeEvent.text,
                     "elem_tab": {},
                     "lib_lst": [],
                     start_tab: -1
                   };
                   this.addNotebook(event.nativeEvent.text, tmp_classeur[this.props.notebook.lastId + 1]);
                   this.setState({setNotebook: this.props.settings.notebook, newNotebook: ''});
                 } catch (e) {
                   throw e;
                 }
               }}
               style = {{flex: 1}}
               placeholder = "Veuiller entre le Nom du classeur"
               value = {this.state.newNotebook}
              />
            </View>
          </View>
        );
      }
      return (
        <View>
          <View>
            <Picker
              selectedValue={this.state.setNotebook}
              onValueChange={(value) => {
                if (value !== null) {
                  this.setState({setNotebook: value});
                } else {
                  this.setState({setNotebook: value});
                }
              }}>
              { classeur_item }
            </Picker>
          </View>
          {setPict}
          {supressNotebook}
          {newNotebookForm}
          {lib_picker}
          {this.renderLibSetting()}
        </View>
      );
    } else {
      return (null);
    }
  }

  renderLibSetting() {
    if (this.state.setlib && this.state.setlib !== null && this.props.notebook[this.state.setNotebook]) {
      if (__DEV__ && debug) {
        console.log(
          this.constructor.name,
          arguments.callee.toString().match(/function ([^\(]+)/)[1],
          /*"props: ",
          this.props,
          "state: ",
          this.state*/
        );
      }

      var validateLibSetting = (
          <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'flex-start',
            marginTop: 10
          }}>
            <Button
              onPress={() => {
                var lib = JSON.parse(JSON.stringify(this.props.lib_lst[this.state.setlib]));
                lib.setting.nb_line = this.state.setlib_nb_line;
                lib.setting.nb_colonne = this.state.setlib_nb_colonne;
                console.log(lib);
                this.props.updateLibLst(this.state.setlib, lib);
                Alert.alert("Paramétres des onglets", "Paramétres pris en compte");
              }}
              title="Valider les paramétre de l'onglet"
              color="green"
              accessibilityLabel="Valider les paramétre de l'onglet"
            />
          </View>
      );
      return (
        <ScrollView>
          <View style={{flexDirection: 'row'}}>
            <Text>Nombre de ligne </Text>
            <TextInput
             keyboardType = 'numeric'
             onChangeText = {(value) => {
               this.setState({setlib_nb_line: value});
             }}
             value = {this.state.setlib_nb_line}
             maxLength = {3}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text>Nombre de colonne </Text>
            <TextInput
             keyboardType = 'numeric'
             onChangeText = {(value) => {
               console.log(value);
               this.setState({setlib_nb_colonne: value});
             }}
             value = {this.state.setlib_nb_colonne}
             maxLength = {3}
            />
          </View>
          {validateLibSetting}
        </ScrollView>
      );
    } else if (this.state.setNotebook !== null && this.state.setlib === null && this.props.notebook[this.state.setNotebook]) {
      var newLib = this.state.newLib;
      return (
        <View>
          <View>
            <Text style={{fontWeight: 'bold', fontSize: 30, textAlign:'center'}}>Creation d'une nouvelle catégorie</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text>Label de la catégorie </Text>
            <TextInput
             keyboardType = 'default'
             onChangeText = {(value) => {
               newLib.text = value.replace(new RegExp('  ', 'g'), ' ');
               newLib.name = value.replace(new RegExp(' ', 'g'), '').replace(new RegExp("'", 'g'), '').toLowerCase();
               this.setState({newLib: newLib});
             }}
             style = {{flex: 1}}
             placeholder = "Veuiller entre le text qui sera afficher"
             value = {this.state.newLib.text}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text>Nombre de ligne </Text>
            <TextInput
             keyboardType = 'numeric'
             onChangeText = {(value) => {
               newLib.setting.nb_line = value;
               this.setState({newLib: newLib});
             }}
             value = {this.state.newLib.setting.nb_line}
             maxLength = {3}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text>Nombre de colonne </Text>
            <TextInput
             keyboardType = 'numeric'
             onChangeText = {(value) => {
               newLib.setting.nb_colonne = value;
               this.setState({newLib: newLib});
             }}
             value = {this.state.newLib.setting.nb_colonne}
             maxLength = {3}
            />
          </View>
          <View>
            <Button
              onPress={() => {
                this.addNewLib(this.state.newLib, this.state.setNotebook)
              }}
              title="Créer"
              color="green"
            />
          </View>
        </View>
      );
    } else {
      return (null);
    }
  }

  updateSettings(tmp = this.state.settings){
    this.props.changeActualNotebook(tmp.notebook);
    this.props.updateSettings(tmp);
    this.setState({dispGlobalSeting: false});
  }

  addNewLib(lib, notebookId){
    if (lib.name === null || !lib.name.localeCompare('')) {
      Alert.alert(
        'Création de catégorie',
        'Labes de la catégorie non spécifier');
    } else {
      var tmpNotebook = JSON.parse(JSON.stringify(this.props.notebook[notebookId]));
      lib.id = this.props.lib_lst.lastId + 1;
      lib.elem_tab = this.props.elem_tab.lastId + 1;
      tmpNotebook.lib_lst.push(lib.id);
      this.props.addElemTab(lib.elem_tab, parseInt(lib.setting.nb_line) * parseInt(lib.setting.nb_colonne));
      this.props.addLibLst(lib.id, lib);
      tmpNotebook.start_tab = tmpNotebook.lib_lst[0];
      this.props.updateNotebook(notebookId, tmpNotebook);
      this.setState({
        newLib: {
          "name": null,
          "text": null,
          "style": {},
          "elem": '',
          "setting": {nb_line: "1", nb_colonne: "2"}
        },
        setlib: lib.id,
        setlib_nb_line: lib.setting.nb_line,
        setlib_nb_colonne: lib.setting.nb_colonne,
      });
    }
  }

  addNotebook(name, elem){
    this.props.addNotebook(name, elem);
    if (!this.props.notebook[this.props.settings.notebook]) {
      var tmp = JSON.parse(JSON.stringify(this.props.settings));
      tmp.notebook = elem.id;
      this.setState({settings: tmp, setNotebook: elem.id});
      this.updateSettings(tmp);
      this.props.changeActualNotebook(elem.id);
    }
  }
}

function mapStateToProps(state) {
  if (__DEV__ && debug) {
    console.log(
      this.constructor.name,
      arguments.callee.toString().match(/function ([^\(]+)/)[1],
      state
    );
  }
  return {
    settings: JSON.parse(JSON.stringify(state.settings)),
    notebook: state.notebook,
    lib_lst: state.lib_lst,
    elem_tab: state.elem_tab,
    sources: state.sources,
    actual_notebook: state.actual_notebook,
    sp_log: state.sp_log
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MaVoixSettings);
