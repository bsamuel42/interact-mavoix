import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

//initData = require('../public/initNotebook');

export const sources = createReducer({}, {
  /*[REHYDRATE](state, action){
    console.log(action);
    return state;
  },*/

  [types.INITIALISE](state, action){
    tmp = action.sources;
    return tmp;
  },

  [types.ADD_SOURCES](state, action){
    let tmp = {};
    tmp[action.id] = action.data;
    tmp["lastId"] = action.id;
    return Object.assign({}, state, tmp);
  },

  [types.UPDATE_SOURCES](state, action){
    tmp = {};
    tmp[action.id] = action.data;
    return Object.assign({}, state, tmp);
  },
});
