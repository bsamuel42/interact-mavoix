import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

var initialName = "Utilisation";

export const notebook = createReducer({}, {
  /*[REHYDRATE](state, action){
    console.log(action);
    return state;
  },*/

  [types.INITIALISE](state, action){
    tmp = action.notebook;
    return tmp;
  },

  [types.ADD_NOTEBOOK](state, action){
    var tmp = {};
    tmp[state["lastId"] + 1] = action.data;
    tmp[state["lastId"] + 1].id = state["lastId"] + 1;
    tmp["lastId"] = state["lastId"] + 1;
    return Object.assign({}, state, tmp);
  },

  [types.DEL_NOTEBOOK](state, action){
    var tmp = JSON.parse(JSON.stringify(state));
    delete tmp[action.id];
    return tmp;
  },

  [types.UPDATE_NOTEBOOK](state, action){
    tmp = JSON.parse(JSON.stringify(state));
    tmp[action.id] = action.data;
    return tmp;
  },

});

export const fullPictLib = createReducer({
  "0": {
    "id": 0,
    "name": "Bibliotéque d'Image",
    "start_tab": 1,
    "lib_lst": [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9
    ]
  }
}, {
  [types.ADD_PICT](state, action){
    var tmp = {};
    return Object.assign({}, state, tmp);
  },
});
