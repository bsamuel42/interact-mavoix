import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

var initialName = "Utilisation";

export const settings = createReducer({}, {
    /*[REHYDRATE](state, action){
      console.log(action);
      return state;
    },*/

    [types.INITIALISE](state, action){
      tmp = action.settings;
      return tmp;
    },

    [types.UPDATE_SETTINGS](state, action){
      return  Object.assign({}, state, action.data);
    }
  }
);
