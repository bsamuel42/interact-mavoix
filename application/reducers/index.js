import { combineReducers } from 'redux';
import * as firstStartReducers from './firstStart';
import * as SettingsReducers from './settings';
import * as NotebookReducers from './notebook';
import * as FullPictLibReducers from './fullPictLib';
import * as ActualNotebookReducers from './actual_notebook';
import * as LibLstReducers from './lib_lst';
import * as ElemTabReducers from './elem_tab';
import * as SourcesReducers from './sources';
import * as SpLogReducers from './sp_log';


export default combineReducers(ActionsCreator = Object.assign({},
  firstStartReducers,
  SettingsReducers,
  NotebookReducers,
  ActualNotebookReducers,
  FullPictLibReducers,
  LibLstReducers,
  ElemTabReducers,
  SourcesReducers,
  SpLogReducers
));
