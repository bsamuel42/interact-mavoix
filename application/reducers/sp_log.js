import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

export const sp_log = createReducer({}, {
  [types.INITIALISE](state, action){
    tmp = {lastId: -1};
    return tmp;
  },

  [types.ADD_SP_LOG](state, action){
    var newId = state["lastId"] + 1;
    var tmp = {
      [newId]: Object.assign({}, action.payload, {id: newId})
    };
    tmp["lastId"] = newId;
    return Object.assign({}, state, tmp);
  },

  [types.UPDATE_SP_LOG](state, action){
    tmp = {};
    Object.keys(state).map((elem, index) => {
      if (state[elem].id == action.id ) {
        tmp[elem] = Object.assign({}, action.payload, {id: action.id});
      }
    });
    return Object.assign({}, state, tmp);
  }
});
