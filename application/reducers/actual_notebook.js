import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

export const actual_notebook = createReducer("0", {
  /*[REHYDRATE](state, action){
    console.log(action);
    return state;
  },*/

  [types.INITIALISE](state, action){
    tmp = JSON.parse(JSON.stringify(action.actual_notebook));
    return tmp;
  },

  [types.CHANGE_ACTUAL_NOTEBOOK](state, action){
    return action.id;
  }
});
