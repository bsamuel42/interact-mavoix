import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

export const first_start = createReducer(true, {
  [types.FIRST_START](state, action){
    return false;
  },
  [types.RESET_APP](state, action){
    return true;
  }
})
