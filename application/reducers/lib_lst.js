import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

export const lib_lst = createReducer({}, {
  /*[REHYDRATE](state, action){
    console.log(action);
    return state;
  },*/

  [types.INITIALISE](state, action){
    tmp = action.lib_lst;
    return tmp;
  },

  [types.ADD_LIB_LST](state, action){
    tmp = JSON.parse(JSON.stringify(state));
    tmp[action.id] = action.data;
    tmp["lastId"] = action.id;
    return tmp;
  },

  [types.UPDATE_LIB_LST](state, action){
    tmp = JSON.parse(JSON.stringify(state));
    tmp[action.id] = action.data;
    return tmp;
  },
});
