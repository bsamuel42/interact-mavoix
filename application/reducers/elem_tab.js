import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

export const elem_tab = createReducer({}, {
  /*[REHYDRATE](state, action){
    console.log(action);
    return state;
  },*/

  [types.INITIALISE](state, action){
    tmp = action.elem_tab;
    return tmp;
  },

  [types.ADD_ELEM_TAB](state, action){
    var tmp = {
      [action.id]: {
        id: action.id,
        value: []
      }
    };
    for (var i = 0; i < action.size; i++) {
      tmp[action.id].value.push({
          "display": false,
          "text": "",
          "source": null,
          "style": {
              "height": 100,
              "width": 100
          }
      });
    }
    tmp["lastId"] = action.id;
    return Object.assign({}, state, tmp);
  },

  [types.UPDATE_ELEM_TAB](state, action){
    tmp = {};
    Object.keys(state).map((elem, index) => {
      if (state[elem].id == action.id ) {
        var value = JSON.parse(JSON.stringify(state[elem].value));
        value[action.pos] = action.data;
        tmp[elem] = Object.assign({}, state[elem], {value: value});
      }
    });
    return Object.assign({}, state, tmp);
  },

  [types.UPDATE_ELEM_TAB_SIZE](state, action){
    var tmpValue = [];
    if (state[action.id].value.length < action.size) {
      for (var i = state[action.id].value.length; i < action.size; i++) {
        tmpValue.push({
            "display": false,
            "text": "",
            "source": null,
            "style": {
                "height": 100,
                "width": 100
            }
        });
      }
      var tmp = JSON.parse(JSON.stringify(state[action.id].value));
      tmpValue = tmp.concat(tmpValue);
    } else if (state[action.id].value.length > action.size) {
      var i = 0;
      state[action.id].value.map((item) => {
        if (item.source != null) {
          tmpValue.push(item);
        } else if (state[action.id].value.length - i <= action.size) {
          tmpValue.push(item);
        } else {
          i++;
        }
      });
      tmpValue = tmpValue.slice(0, action.size);
    } else {
      tmpValue = state[action.id].value;
    }
    var tmp = {
      [action.id]: {
        id: action.id,
        value: tmpValue
      }
    };
    return Object.assign({}, state, tmp);
  }

});
