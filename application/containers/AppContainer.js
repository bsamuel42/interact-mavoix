import React, { Component } from 'react';
import {
  Navigator,
  View,
  TouchableHighlight,
  AsyncStorage,
  Dimensions,
  Image,
  Text
} from 'react-native';

import {persistStore, autoRehydrate} from 'redux-persist';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions';
import genInitData from '../lib/genInitData';

//const initData = require('../public/initNotebook');

import styles from '../styles/styles';
import LoadingPage from '../components/LoadingPage';
import Use from '../components/Use';
import Admin from '../components/Admin';
import SetNotebookPict from '../components/SetNotebookPict';

const debug = false;

class AppContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rehydrated: false,
      first: true
    }
  }

  componentWillMount(){
    if (__DEV__ && debug) {
      console.log(
        this.constructor.name,
        arguments.callee.toString().match(/function ([^\(]+)/)[1],
        /*"props: ",
        this.props,
        "state: ",
        this.state*/
      );
    }
    persistStore(this.props.store, {storage: AsyncStorage, blacklist: ['sp_area']}, () => {
      this.setState({first: this.props.first_start});
      if (this.props.first_start) {
        var init = genInitData();
        init.pictPromis.then(() => {
          console.log("all pict downloaded", init.initData);
          this.props.firstStart(init.initData);
          this.setState({ rehydrated: true });
        }).catch((error) => {
          console.error("catched error on download pict promise", error);
        })
      } else {
        this.setState({ rehydrated: true });
      }
    });
  }

  renderScene(route, navigator) {
    switch (route.name) {
      case 'Use':
        return (
          <View>
            <Use
              {...route.props}
              store={route.store}
              navigator={navigator}
              {...route.passProps}
            />
          </View>);
      case 'Admin':
        return (
        <View>
          <Admin
            {...route.props}
            store={route.store}
            navigator={navigator}
            first={route.passProps.first}
            {...route.passProps}
          />
        </View>);
      case 'SetNotebookPict':
        return (
          <View>
            <SetNotebookPict
              {...route.props}
              notebookToSet={route.notebookToSet}
              navigator={navigator}
              {...route.passProps}
            />
          </View>);
      default:
        return (
          <View>
            <Use {...route.props} store={route.store} navigator={navigator} {...route.passProps} />
          </View>);
    }
  }

  render() {
    console.log("render");
    if (this.state.rehydrated) {
      return (
        <Navigator
          style={{ flex:1 }}
          initialRoute={{
            name: (this.state.first) ? 'Admin' : 'Use',
            props: this.props,
            store: this.props.store,
            passProps: {first: this.state.first}
          }}
          renderScene={ this.renderScene } />
      );
    } else {
      return (
        <LoadingPage first={this.state.first}/>
      );
    }
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    first_start: state.first_start,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
