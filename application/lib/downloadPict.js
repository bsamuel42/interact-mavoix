import React from 'react'

import {
  CameraRoll,
} from 'react-native';

import RNFetchBlob from 'react-native-fetch-blob';

export default function downloadPict(uri, name) {
  let dirs = RNFetchBlob.fs.dirs;
  var conf = null;
  if (name) {
    conf = {
      fileCache : true,
      appendExt : 'png',
      path : dirs.PictureDir + '/InteractMaVoix/' + name + '.png'
    };
  }

  if (__DEV__){
    //console.log("downloadPict", uri, dirs.PictureDir);
  }
  return RNFetchBlob.fs.exists(conf.path)
  .then((exist) => {
      if (!exist) {
        return RNFetchBlob.config(conf)
        .fetch('GET', uri, {
          //some headers ..
        }).then((res) => {
          RNFetchBlob.fs.scanFile([ { path : res.path()} ], 'image/png');
          return res;
        }).catch(function(error) {
          console.log("downloadPict error", error);
        });
      } else {
        return new Promise(function(resolve, reject) {
          const res = {path: () => {return conf.path}};
          resolve(res);
        });
      }
  })
}
