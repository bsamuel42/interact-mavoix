import downloadPict from '../lib/downloadPict';
import {
  Platform,
} from 'react-native';



/*function base64FromUrl(url, id, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    var reader = new FileReader();
    reader.onloadend = function() {
      callback(reader.result, id);
    }
    reader.readAsDataURL(xhr.response);
  };
  xhr.open('GET', url);
  xhr.responseType = 'blob';
  xhr.send();
}*/

function genInitData(classeur = "Utilisation"){
  var pictPromis = [];
  var notebookId = 0;
  var lib_lstId = 1;
  var elem_tabId = 1;
  var sourcesId = 1;

  var initData = {
    settings: {
      learningMode: false,
      notebook: 0,
      auth: {
        name: undefined,
        pass: undefined,
      },
      color: {
        header: "#FFFFFF",
        body: "#3A947C",
        settingBody: "#D9D8D8",
        speakAreaColor: "#2C5C6D",
      },
      speak_area_size: "1",
      selectMode: "0", // 0 click, 1 drag and drop, 2 les deux
      language: "fr",
      sound: false,
      text: {
        show: true,
        style: {
          fontSize: 25,
        }
      },
      sorting: 0, // 0 disable, 1 manual (by user), 2 random
      tabnav: 0, // 0 click, 1 scroll, 2 les deux
    },
    notebook: {},
    lib_lst: {},
    elem_tab: {},
    sources: {},
    fullPictLib: {},
  };

  initData.fullPictLib[notebookId] = {
    "id": notebookId,
    "name": "Bibliotéque d'Image",
    "start_tab": lib_lstId,
    "lib_lst": [],
  }
  var libName = ["Activites", "Adjectifs","Aliments","Animaux", "Boissons", "Jeux d'exterieurs", "Jouets d'interieur", "Verbes", "Vetements"];
  var libSizemin = [1, 9, 27, 49, 52, 59, 75, 92, 121];
  var libSizemax = [8, 26, 48, 51, 58, 69, 91, 120, 131];

  var text = [
    ["chanter", "écouter de la musique", "galipette", "ordinateur portable", "dessin", "football", "lire1", "se promener2"],
    ["0", "2", "4", "6", "8", "blanc", " jaune", "noir", "rouge", "1", "3", "5", "7", "9", "bleu", "moyen", "orange", "vert"],
    ["abricot", "croissant", "gâteaux", "œufs à la poêle", " paquet de chips", "viande", "banane", "fraise", " glace", "orange", "pomme de terre", "yaourt", "bonbon", "fromage", "kiwi", " pain", "pomme", "chocolat", "fruits", " légumes", "paquet de biscuits", "rondelle d'orange"],
    ["chat", "chien", "poisson"],
    ["Coca-Cola", "eau", "jus de pomme", "jus de raisin", "jus d'orange", "lait au cacao", "lait"],
    ["balançoire", "balançoire", "ballon", "patins", "toboggan", "trampoline", "trampoline", " vélo", "parc", "raquette", "trottinette"],
    ["avion", "camion de pompiers", "dessins animés", "gommettes", "marionettes", "toupies", "ballon", "camions", "dinosaures", "jouer le piano", "peinture", "voiture", "bulles de savon", "bulles de savon", "console", "feutres", " livre", "peinture rupestre", "cubes empilables", "garage", "marionettes", " télévision"],
    ["allumer", "allumer", "boire", "chatouiller", "chatouiller", "courir", "courir", "donner", "donner", "donner un bisou", "donner un bisou", "éteindre", "éteindre", "fermer", "fermer", "glisser", "manger", "ouvrir", "ouvrir", "pousser", "rouler", "sauter", "sauter", "sortir", "sortir", "souffler", "tirer", "tirer", "tourner"],
    ["casquette", "chapeau", "chaussure", "chaussure de sport", "chaussures", "chaussures", "écharpe", "imperméable", "veste", "veste"]
  ];

  var id = 1;

  libName.map((lib_text, index) => {
    var tmp_txt = lib_text.replace(new RegExp(' ', 'g'), '').replace(new RegExp("'", 'g'), '').toLowerCase()
    var elemTabValue = [];
    text[index].map((elem_name) => {
      const cb_id = sourcesId;
      pictPromis.push(downloadPict("http://interact-autism.fr/static/ma-voix/image/" + id + ".png", id).then((res) => {
        var source = {
          "id": cb_id,
          //"uri": dataUrl
          "uri": Platform.OS === 'android' ? 'file://' + res.path()  : '' + res.path()
        };
        initData.sources[cb_id] = source;
      }));
      elemTabValue.push({
        "text": elem_name,
        "source": sourcesId,
        "style": {height:100, width: 100},
        "display": true
      });
      sourcesId++;
      id++;
    }, this);
    var lib_tmp_elme = {
      "id": lib_lstId,
      "name": tmp_txt,
      "text": lib_text,
      "style": {},
      "setting": {
          "nb_line": "10",
          "nb_colonne": "5"
      },
      "elem_tab": elem_tabId,
    };
    initData.lib_lst[lib_lstId] = lib_tmp_elme;//.push(lib_tmp_elme);
    initData.elem_tab[elem_tabId] = {"id": elem_tabId, "value": elemTabValue};
    initData.fullPictLib[notebookId].lib_lst.push(lib_lstId);
    lib_lstId++;
    elem_tabId++;
  });
  initData.lib_lst["lastId"] = lib_lstId - 1;
  initData.elem_tab["lastId"] = elem_tabId - 1;
  initData.sources["lastId"] = sourcesId - 1;
  initData.notebook["lastId"] = notebookId;

  initData.notebook[notebookId]= {
    "id": notebookId,
    "name": classeur,
    "start_tab": -1,
    "lib_lst": [],
  }



  return ({initData: initData, pictPromis: Promise.all(pictPromis)});
}

//var initData = genInitData();
export default genInitData;
