import * as types from './types';

export function changeActualNotebook(id) {
  return {
    type: types.CHANGE_ACTUAL_NOTEBOOK,
    id: id
  }
}
