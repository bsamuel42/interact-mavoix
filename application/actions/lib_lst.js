import * as types from './types';

export function addLibLst(id, data) {
  return {
    type: types.ADD_LIB_LST,
    id: id,
    data: data
  }
}

export function updateLibLst(id, data) {
  /*return {
    type: types.UPDATE_LIB_LST,
    id: id,
    data: data
  }*/
  return function (dispatch){
    dispatch({
      type: types.UPDATE_LIB_LST,
      id: id,
      data: data
    });
    dispatch({
      type: types.UPDATE_ELEM_TAB_SIZE,
      id: id,
      size: parseInt(data.setting.nb_line) * parseInt(data.setting.nb_colonne)
    });
  }
}
