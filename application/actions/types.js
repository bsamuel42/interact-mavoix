export const FIRST_START = 'FIRST_START';
export const RESET_APP = 'RESET_APP';
export const INITIALISE = 'INITIALISE';

export const UPDATE_SETTINGS = 'UPDATE_SETTINGS';

export const ADD_NOTEBOOK = 'ADD_NOTEBOOK';
export const DEL_NOTEBOOK = 'DEL_NOTEBOOK';
export const ADD_TAB = 'ADD_TAB';
export const DEL_TAB = 'DEL_TAB';
export const ADD_PICT = 'ADD_PICT';
export const CHANGE_ACTUAL_NOTEBOOK = 'CHANGE_ACTUAL_NOTEBOOK';
export const UPDATE_NOTEBOOK = 'UPDATE_NOTEBOOK';

export const UPDATE_LIB_LST = 'UPDATE_LIB_LST';
export const ADD_LIB_LST = 'ADD_LIB_LST';

export const ADD_SOURCES = 'ADD_SOURCES';
export const UPDATE_SOURCES = 'UPDATE_SOURCES';

export const UPDATE_ELEM_TAB = 'UPDATE_ELEM_TAB';
export const ADD_ELEM_TAB = 'ADD_ELEM_TAB';
export const UPDATE_ELEM_TAB_SIZE = 'UPDATE_ELEM_TAB_SIZE';

export const SP_AREA_ADD_PICT = 'SP_AREA_ADD_PICT';
export const SP_AREA_DEL_PICT = 'SP_AREA_DEL_PICT';

export const ADD_SP_LOG = 'ADD_SP_LOG';
export const UPDATE_SP_LOG = 'UPDATE_SP_LOG';
