import * as types from './types';

export function addNotebook(name, data) {
  return {
    type: types.ADD_NOTEBOOK,
    name: name,
    id: data.id,
    data: data
  }
}

export function delNotebook(id){
  return {
      type: types.DEL_NOTEBOOK,
      id: id
  };
}

export function updateNotebook(id, data) {
  return {
    type: types.UPDATE_NOTEBOOK,
    id: id,
    data: data
  }
}
