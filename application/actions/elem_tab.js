import * as types from './types';

export function addElemTab(id, size) {
  return {
    type: types.ADD_ELEM_TAB,
    id: id,
    size: size
  }
}

export function updateElemTab(id, data, pos) {
  return {
    type: types.UPDATE_ELEM_TAB,
    id: id,
    pos: pos,
    data: data
  }
}
