import * as types from './types';
import * as actions from '../../application/actions';

export function firstStart(data) {
  return function(dispatch) {
    if (__DEV__) {
      //console.log(JSON.stringify(data, null, 2));
    }
    dispatch({
      type: types.FIRST_START,
    });
    dispatch({
      type: types.INITIALISE,
      sources: data.sources,
      settings: data.settings,
      notebook: data.notebook,
      lib_lst: data.lib_lst,
      elem_tab: data.elem_tab,
      actual_notebook: (__DEV__) ? 0 : 1,
    });
  }
}

export function resetApp() {
  return {
    type: types.RESET_APP,
  }
}
