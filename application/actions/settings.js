import * as types from './types';

export function updateSettings(settings) {
  return {
    type: types.UPDATE_SETTINGS,
    data: settings
  }
}
