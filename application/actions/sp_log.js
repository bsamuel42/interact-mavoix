import * as types from './types';

export function addSpLog(data) {
  return {
    type: types.ADD_SP_LOG,
    payload: data
  }
}

export function updateSpLog(id, data) {
  return {
    type: types.UPDATE_SP_LOG,
    payload: data,
    id: id
  }
}
