import * as FirstStartActions from './firstStart';
import * as SettingsActions from './settings';
import * as NotebookActions from './notebook';
import * as ActualNotebookActions from './actual_notebook';
import * as LibLstActions from './lib_lst';
import * as ElemTabActions from './elem_tab';
import * as SourcesActions from './sources';
import * as SpLogActions from './sp_log';

export const ActionCreators = Object.assign({},
  FirstStartActions,
  SettingsActions,
  NotebookActions,
  ActualNotebookActions,
  LibLstActions,
  ElemTabActions,
  SourcesActions,
  SpLogActions
);
