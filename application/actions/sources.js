import * as types from './types';

export function addSources(id, data) {
  return {
    type: types.ADD_SOURCES,
    id: id,
    data: data
  }
}

export function updateSources(id, data) {
  return {
    type: types.UPDATE_SOURCES,
    id: id,
    data: data
  }
}
