import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../application/actions/sp_log';
import * as types from '../../application/actions/types';

var testedAction = '';

describe('elem_tab actions', () => {
  it('should create an action to Add a sp log elem', () => {
    testedAction = 'addSpLog';
    const data = {
      start_time: null,
      end_time: null,
      stages: []
    };
    const expectedAction = {
      type: types.ADD_SP_LOG,
      payload: data
    }
    expect(actions[testedAction](data)).toEqual(expectedAction)
  });

  it('should create an action to Update a sp log elem', () => {
    testedAction = 'updateSpLog';
    const id = "1";
    const data = {
      start_time: null,
      end_time: null,
      stages: []
    };
    const expectedAction = {
      type: types.UPDATE_SP_LOG,
      payload: data,
      id: id
    }
    expect(actions[testedAction](id, data)).toEqual(expectedAction)
  })
})
