import * as actions from '../../application/actions/actual_notebook';
import * as types from '../../application/actions/types';

describe('actual_notebook actions', () => {
  it('should create an action to change the Actual Notebook', () => {
    const id = "2";
    const expectedAction = {
      type: types.CHANGE_ACTUAL_NOTEBOOK,
      id: id,
    }
    expect(actions.changeActualNotebook(id)).toEqual(expectedAction)
  })

})
