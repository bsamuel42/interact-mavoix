import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../application/actions/sources';
import * as types from '../../application/actions/types';

var testedAction = '';

describe('sources actions', () => {
  it('should create an action to Add a source', () => {
    testedAction = 'addSources';
    const id = "1";
    const data = {
      "id": 1,
      "uri": "uriTest"
    };

    const expectedAction = {
      type: types.ADD_SOURCES,
      id: id,
      data: data
    }
    expect(actions[testedAction](id, data)).toEqual(expectedAction)
  });

  it('should create an action to Update a source', () => {
    testedAction = 'updateSources';
    const id = "1";
    const data = {
      "id": 1,
      "uri": "uriTest"
    };
    const expectedAction = {
      type: types.UPDATE_SOURCES,
      data: data,
      id: id,
    }
    expect(actions[testedAction](id, data)).toEqual(expectedAction)
  })
})
