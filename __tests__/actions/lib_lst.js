import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../application/actions/lib_lst';
import * as types from '../../application/actions/types';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

var testedAction = '';

describe('lib_lst actions', () => {
  it('should create an action to Add an elem_tab', () => {
    testedAction = 'addLibLst';
    const id = "1";
    const data = {
      "id": 1,
      "name": "activites",
      "text": "Activites",
      "style": {},
      "setting": {
        "nb_line": "10",
        "nb_colonne": "5"
      },
      "elem_tab": 1
    };

    const expectedAction = {
      type: types.ADD_LIB_LST,
      id: id,
      data: data
    }
    expect(actions[testedAction](id, data)).toEqual(expectedAction)
  });

  it('should create an action to Update an elem_tab', () => {
    testedAction = 'updateLibLst';
    const id = "1";
    const data = {
      "id": 1,
      "name": "activites",
      "text": "Activites",
      "style": {},
      "setting": {
        "nb_line": "10",
        "nb_colonne": "5"
      },
      "elem_tab": 1
    };
    const expectedAction = [
      {
        type: types.UPDATE_LIB_LST,
        data: data,
        id: id,
      },
      {
        type: types.UPDATE_ELEM_TAB_SIZE,
        id: id,
        size: parseInt(data.setting.nb_line) * parseInt(data.setting.nb_colonne)
      }
    ]

    const store = mockStore({});

    store.dispatch(actions[testedAction](id, data));
    expect(store.getActions()).toEqual(expectedAction)
  })
})
