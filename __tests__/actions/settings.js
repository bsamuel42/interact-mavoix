import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../application/actions/settings';
import * as types from '../../application/actions/types';

var testedAction = '';

describe('sttings actions', () => {
  it('should create an action to Update app settings', () => {
    testedAction = 'updateSettings';
    const id = "1";
    const data = {"settings": {}};

    const expectedAction = {
      type: types.UPDATE_SETTINGS,
      data: data
    }
    expect(actions[testedAction](data)).toEqual(expectedAction)
  });
})
