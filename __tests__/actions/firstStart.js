import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../application/actions/firstStart';
import * as types from '../../application/actions/types';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

var testedAction = '';

describe('first Start actions', () => {
  it('should create an action to Initialise the ap at first Start', () => {
    testedAction = 'firstStart';
    const data = {
      "settings": {},
      "notebook": {},
      "lib_lst": {},
      "elem_tab": {},
      "sources": {},
      "actual_notebook": 0,
    };

    const expectedAction = [{
      type: types.FIRST_START,
    },
    {
      type: types.INITIALISE,
      sources: data.sources,
      settings: data.settings,
      notebook: data.notebook,
      lib_lst: data.lib_lst,
      elem_tab: data.elem_tab,
      actual_notebook: data.actual_notebook,
    }]
    const store = mockStore({});

    store.dispatch(actions[testedAction](data));
    expect(store.getActions()).toEqual(expectedAction)
  });

})
