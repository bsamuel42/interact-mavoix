import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../application/actions/notebook';
import * as types from '../../application/actions/types';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('notebook actions', () => {
  it('should create an action to add a Notebook', () => {
    const name = 'Utilisation';
    const data = {
        "id": 1,
        "name": "Utilisation",
        "start_tab": 1,
        "lib_lst": [ 1, 2, 3]
    };
    const expectedAction = {
      type: types.ADD_NOTEBOOK,
      name: name,
      id: data.id,
      data: data
    }
    expect(actions.addNotebook(name, data)).toEqual(expectedAction)
  })

  it('should create an action to del a Notebook', () => {
    const id = 0;

    const expectedActions = {
      type: types.DEL_NOTEBOOK,
      id: id
    };
     const store = mockStore({
       notebook: {
         "lastId": 0,
         "0": {
           "id": 0,
           "name": "Utilisation",
           "start_tab": 1,
           "lib_lst": [ 1, 2, 3]
         }
       }
     });
     expect(actions.delNotebook(id)).toEqual(expectedActions)
     /*return store.dispatch(actions.delNotebook(id)).then(() => {
       // return of async actions
       expect(store.getActions()).toEqual(expectedActions)
     });*/
   });

  it('should create an action to update a Notebook', () => {
    const data = {
       "id": 1,
       "name": "Utilisation",
       "start_tab": 1,
       "lib_lst": [ 1, 2, 3]
   };
   const expectedAction = {
     type: types.UPDATE_NOTEBOOK,
     id: data.id,
     data: data
   }
   expect(actions.updateNotebook(data.id, data)).toEqual(expectedAction)
  })

})
