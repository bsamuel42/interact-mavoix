import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../application/actions/elem_tab';
import * as types from '../../application/actions/types';

var testedAction = '';

describe('elem_tab actions', () => {
  it('should create an action to Add an elem_tab', () => {
    testedAction = 'addElemTab';
    const id = "1";
    const size = 2;
    const expectedAction = {
      type: types.ADD_ELEM_TAB,
      id: id,
      size: size
    }
    expect(actions[testedAction](id, size)).toEqual(expectedAction)
  });

  it('should create an action to Update an elem_tab', () => {
    testedAction = 'updateElemTab';
    const id = "1";
    const data = {
      "text": "chanter",
      "source": 1,
      "style": {
        "height": 100,
        "width": 100
      },
      "display": true
    };
    const pos = 2;
    const expectedAction = {
      type: types.UPDATE_ELEM_TAB,
      data: data,
      id: id,
      pos: pos
    }
    expect(actions[testedAction](id, data, pos)).toEqual(expectedAction)
  })
})
