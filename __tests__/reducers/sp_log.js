import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/sp_log';
import * as actions from '../../application/actions/sp_log';
var deepFreeze = require('deep-freeze');

const curentFile = 'sp_log';
const testedReducer = reducer[curentFile];

const initData = {};

describe('SP_LOG reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(initData);
  })

  it('should handle INITIALISE', () => {
    var initialState = {};
    deepFreeze(initialState);

    expect(
      testedReducer(initialState, {
        type: types.INITIALISE,
      })
    ).toEqual({
      "lastId": -1
    });
  });

  it('should handle ADD_SP_LOG lastId: -1 ', () => {
    var initialState = {lastId: -1};
    deepFreeze(initialState);

    const data = {
      start_time: null,
      end_time: null,
      stages: []
    };

    expect(
      testedReducer(initialState, {
        type: types.ADD_SP_LOG,
        payload: data
      })
    ).toEqual({
      "lastId": 0,
      0: {
        id: 0,
        start_time: null,
        end_time: null,
        stages: []
      }
    });
  });

  it('should handle ADD_SP_LOG lastId: 0 ', () => {
    var initialState = {
      lastId: 0,
      0: {
        id: 0,
        start_time: null,
        end_time: null,
        statge: [{source: 1}]
      }
    };
    deepFreeze(initialState);

    const data = {
      start_time: null,
      end_time: null,
      stages: [{source: 2}]
    };

    expect(
      testedReducer(initialState, {
        type: types.ADD_SP_LOG,
        payload: data
      })
    ).toEqual(Object.assign({}, initialState, {
      1: {
        id: 1,
        start_time: null,
        end_time: null,
        stages: [{source: 2}]
      },
      lastId: 1
    }));
  });

  it('should handle UPDATE_SP_LOG', () => {
    var initialState = {
      lastId: 0,
      0: {
        id: 0,
        start_time: null,
        end_time: null,
        statge: [{source: 1}]
      }
    };
    deepFreeze(initialState);

    const data = {
      start_time: 9,
      end_time: 10,
      stages: [{source: 2}]
    };

    expect(
      testedReducer(initialState, {
        type: types.UPDATE_SP_LOG,
        payload: data,
        id: 0
      })
    ).toEqual({
      0: {
        id: 0,
        start_time: 9,
        end_time: 10,
        stages: [{source: 2}]
      },
      lastId: 0
    });
  });

});
