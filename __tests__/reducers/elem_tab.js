import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/elem_tab';
import * as actions from '../../application/actions/elem_tab';
var deepFreeze = require('deep-freeze');

const curentFile = 'elem_tab';
const testedReducer = reducer[curentFile];

const initData = {};

describe('elem_tab reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(initData);
  })

  it('should handle INITIALISE', () => {
    var initialState = {};
    deepFreeze(initialState);
    const data = {
      "id": 1,
      "value": [
        {
          "text": "chanter",
          "source": 1,
          "style": {
            "height": 100,
            "width": 100
          },
          "display": true
        }
      ]
    };

    expect(
      testedReducer(initialState, {
        type: types.INITIALISE,
        elem_tab: {
          "1": data,
          "lastId": 1
        },
      })
    ).toEqual({
      "1": data,
      "lastId": 1
    });
  });

  it('should handle ADD_ELEM_TAB', () => {
    var initialState = {
      "1": {
        "id": 1,
        "value": [
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          }
        ]
      },
      "lastId": 1
    };
    deepFreeze(initialState);
    const data = {
      "1": {
        "id": 1,
        "value": [
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          }
        ]
      },
      "3": {
        "id": 3,
        "value": [
          {
            "text": "",
            "source": null,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": false
          }
        ]
      },
      "lastId": 3
    }
    const id = 3;
    const size = 1;

    expect(
      testedReducer(initialState, actions.addElemTab(id, size))
    ).toEqual(data);
  });

  it('should handle UPDATE_ELEM_TAB', () => {
    var initialState = {
      "1": {
        "id": 1,
        "value": [
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          },
          {
            "text": "",
            "source": null,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          }
        ]
      },
      "lastId": 1
    };
    deepFreeze(initialState);
    const data = {
      "text": "manger",
      "source": 5,
      "style": {
        "height": 200,
        "width": 200
      },
      "display": true
    }
    const id = 1;
    const pos = 1;

    expect(
      testedReducer(initialState, actions.updateElemTab(id, data, pos))
    ).toEqual({
      "1": {
        "id": 1,
        "value": [
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          },
          data
        ]
      },
      "lastId": 1
    });
  });

  it('should handle UPDATE_ELEM_TAB_SIZE length > size', () => {
    var initialState = {
      "1": {
        "id": 1,
        "value": [
          {
            "text": "",
            "source": null,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          },
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          },
          {
            "text": "",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          }
        ]
      },
      "lastId": 1
    };
    deepFreeze(initialState);

    const id = 1;
    var action = {
      type: types.UPDATE_ELEM_TAB_SIZE,
      id: id,
      size: 1
    };

    expect(
      testedReducer(initialState, action)
    ).toEqual({
      "1": {
        "id": 1,
        "value": [
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          }
        ]
      },
      "lastId": 1
    });

    var action = {
      type: types.UPDATE_ELEM_TAB_SIZE,
      id: id,
      size: 2
    };

    expect(
      testedReducer(initialState, action)
    ).toEqual({
      "1": {
        "id": 1,
        "value": [
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          },
          {
            "text": "",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          }
        ]
      },
      "lastId": 1
    });
  });

  it('should handle UPDATE_ELEM_TAB_SIZE length = size', () => {
    var initialState = {
      "1": {
        "id": 1,
        "value": [
          {
            "text": "",
            "source": null,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          },
          {
            "text": "chanter",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          },
          {
            "text": "",
            "source": 1,
            "style": {
              "height": 100,
              "width": 100
            },
            "display": true
          }
        ]
      },
      "lastId": 1
    };
    deepFreeze(initialState);

    const id = 1;
    var action = {
      type: types.UPDATE_ELEM_TAB_SIZE,
      id: id,
      size: 3
    };

    expect(
      testedReducer(initialState, action)
    ).toEqual(initialState);

    var action = {
      type: types.UPDATE_ELEM_TAB_SIZE,
      id: id,
      size: 2
    };
  });

});
