import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/firstStart';
import * as actions from '../../application/actions/firstStart';
var deepFreeze = require('deep-freeze');

const testedReducer = reducer.first_start;
const initData = "0";

describe('firstStart reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(true);
  })

  it('should handle FIRST_START', () => {
    var initialState = true;
    deepFreeze(initialState);

    const data = {
      "settings": {},
      "notebook": {},
      "lib_lst": {},
      "elem_tab": {},
      "sources": {},
      "actual_notebook": 0,
    };

    expect(
      testedReducer(initialState, {
        type: types.FIRST_START,
      })
    ).toEqual(false);
  });

});
