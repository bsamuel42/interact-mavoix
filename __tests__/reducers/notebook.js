import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/notebook';
import * as actions from '../../application/actions/notebook';
var deepFreeze = require('deep-freeze');

const testedReducer = reducer.notebook;
const initData = {};

describe('notebook reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(initData);
  })

  it('should handle INITIALISE', () => {
    var initialState = {};
    deepFreeze(initialState);
    const data = {
        "id": 1,
        "name": "Utilisation",
        "start_tab": 1,
        "lib_lst": [ 1, 2, 3]
    }

    expect(
      testedReducer(initialState, {
        type: types.INITIALISE,
        notebook: {
          "1": data,
          "lastId": 1
        },
      })
    ).toEqual({
      "1": data,
      "lastId": 1
    });
  });

  it('should handle ADD_NOTEBOOK', () => {
    var initialState = {"lastId": -1};
    deepFreeze(initialState);

    var name = 'Utilisation';
    var data = {
        "id": 0,
        "name": "Utilisation",
        "start_tab": 1,
        "lib_lst": [ 1, 2, 3]
    };
    expect(
      testedReducer(initialState, actions.addNotebook(name, data))
    ).toEqual({
      "lastId": 0,
      "0": {
        "id": 0,
        "name": "Utilisation",
        "start_tab": 1,
        "lib_lst": [ 1, 2, 3]
      }
    });
  });

  it('should handle DEL_NOTEBOOK', () => {
    var initialState = {
      "lastId": 0,
      "0": {
        "id": 0,
        "name": "Utilisation",
        "start_tab": 1,
        "lib_lst": [ 1, 2, 3]
      }
    };
    deepFreeze(initialState);

    expect(
      testedReducer(initialState, actions.delNotebook(0))
    ).toEqual({
      "lastId": 0,
    });
  });

  it('should handle UPDATE_NOTEBOOK', () => {
    var initialState = {
      "lastId": 0,
      "0": {
        "id": 0,
        "name": "Utilisation",
        "start_tab": 1,
        "lib_lst": [ 1, 2, 3]
      }
    };
    deepFreeze(initialState);
    const data = {
      "id": 0,
      "name": "Utilisation",
      "start_tab": 2,
      "lib_lst": [ 1, 2, 3, 5]
    };

    expect(
      testedReducer(initialState, actions.updateNotebook(data.id, data))
    ).toEqual({
      "lastId": 0,
      "0": {
        "id": 0,
        "name": "Utilisation",
        "start_tab": 2,
        "lib_lst": [ 1, 2, 3, 5]
      }
    });
  });

});
