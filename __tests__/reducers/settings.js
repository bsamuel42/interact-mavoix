import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/settings';
import * as actions from '../../application/actions/settings';
var deepFreeze = require('deep-freeze');

const testedReducer = reducer.settings;

const initData = {};

describe('settings reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(initData);
  })

  it('should handle INITIALISE', () => {
    var initialState = {};
    deepFreeze(initialState);
    const data = initData;

    expect(
      testedReducer(initialState, {
        type: types.INITIALISE,
        settings: data,
      })
    ).toEqual(data);
  });

  it('should handle UPDATE_SETTINGS', () => {
    var initialState = initData;
    deepFreeze(initialState);
    const data = Object.assign({}, initData, {"notebook": 1});

    expect(
      testedReducer(initialState, actions.updateSettings(data))
    ).toEqual(data);
  });

});
