import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/actual_notebook';
import * as actions from '../../application/actions/actual_notebook';
var deepFreeze = require('deep-freeze');

const testedReducer = reducer.actual_notebook;
const initData = "0";

describe('actual_notebook reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(initData);
  })

  it('should handle INITIALISE', () => {
    var initialState = '';
    deepFreeze(initialState);
    const data = "1"

    expect(
      testedReducer(initialState, {
        type: types.INITIALISE,
        actual_notebook: data,
      })
    ).toEqual(data);
  });

  it('should handle CHANGE_ACTUAL_NOTEBOOK', () => {
    var initialState = "0";
    deepFreeze(initialState);

    const id = "1";

    expect(
      testedReducer(initialState, actions.changeActualNotebook(id))
    ).toEqual(id);
  });

});
