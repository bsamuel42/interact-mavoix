import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/lib_lst';
import * as actions from '../../application/actions/lib_lst';
var deepFreeze = require('deep-freeze');

const curentFile = 'lib_lst';
const testedReducer = reducer[curentFile];

const initData = {};

describe('lib_lst reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(initData);
  })

  it('should handle INITIALISE', () => {
    var initialState = {};
    deepFreeze(initialState);
    const data = {
      "id": 1,
      "name": "activites",
      "text": "Activites",
      "style": {},
      "setting": {
        "nb_line": "10",
        "nb_colonne": "5"
      },
      "elem_tab": 1
    };

    expect(
      testedReducer(initialState, {
        type: types.INITIALISE,
        lib_lst: {
          "1": data,
          "lastId": 1
        },
      })
    ).toEqual({
      "1": data,
      "lastId": 1
    });
  });

  it('should handle ADD_LIB_LST', () => {
    var initialState = {
      "1": {
        "id": 1,
        "name": "activites",
        "text": "Activites",
        "style": {},
        "setting": {
          "nb_line": "10",
          "nb_colonne": "5"
        },
        "elem_tab": 1
      },
      "lastId": 1
    };
    deepFreeze(initialState);
    const data = {
      "id": 2,
      "name": "toto",
      "text": "Toto",
      "style": {},
      "setting": {
        "nb_line": "1",
        "nb_colonne": "2"
      },
      "elem_tab": 1
    };
    const id = 2;
    const size = 1;

    expect(
      testedReducer(initialState, actions.addLibLst(id, data))
    ).toEqual({
      "1": {
        "id": 1,
        "name": "activites",
        "text": "Activites",
        "style": {},
        "setting": {
          "nb_line": "10",
          "nb_colonne": "5"
        },
        "elem_tab": 1
      },
      "2" : data,
      "lastId": 2
    });
  });

  it('should handle UPDATE_LIB_LST', () => {
    var initialState = {
      "1": {
        "id": 1,
        "name": "activites",
        "text": "Activites",
        "style": {},
        "setting": {
          "nb_line": "10",
          "nb_colonne": "5"
        },
        "elem_tab": 1
      },
      "lastId": 1
    };
    deepFreeze(initialState);
    const data = {
      "id": 1,
      "name": "toto",
      "text": "Toto",
      "style": {},
      "setting": {
        "nb_line": "1",
        "nb_colonne": "2"
      },
      "elem_tab": 1
    };
    const id = 1;

    expect(
      testedReducer(initialState, {
        type: types.UPDATE_LIB_LST,
        id: id,
        data: data
      })
    ).toEqual({
      "1": data,
      "lastId": 1
    });
  });

});
