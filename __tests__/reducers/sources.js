import * as types from '../../application/actions/types';
import * as reducer from '../../application/reducers/sources';
import * as actions from '../../application/actions/sources';
var deepFreeze = require('deep-freeze');

const curentFile = 'sources';
const testedReducer = reducer[curentFile];

const initData = {};

describe('sources reducer', () => {
  it('should return the initial state', () => {
    expect(testedReducer(undefined, {})).toEqual(initData);
  });

  it('should handle INITIALISE', () => {
    var initialState = {};
    deepFreeze(initialState);
    const data = {
      "id": 1,
      "uri": "uriTestId1"
    }
    const id = 1;

    expect(
      testedReducer(initialState, {
        type: types.INITIALISE,
        sources: {
          "1": data,
          "lastId": 1
        },
      })
    ).toEqual({
      "1": data,
      "lastId": 1
    });
  });


  it('should handle ADD_SOURCES', () => {
    var initialState = {
      "lastId": 0
    };
    deepFreeze(initialState);
    const data = {
      "id": 1,
      "uri": "uriTestId1"
    }
    const id = 1;

    expect(
      testedReducer(initialState, actions.addSources(id, data))
    ).toEqual({
      "1": data,
      "lastId": 1
    });
  });

  it('should handle UPDATE_SOURCES', () => {
    var initialState = {
      "1": { "id": 1, "uri": "uriTestId1"},
      "lastId": 1
    };
    deepFreeze(initialState);
    const data = {
      "id": 1,
      "uri": "an other uri"
    }
    const id = 1;

    expect(
      testedReducer(initialState, actions.updateSources(id, data))
    ).toEqual({
      "1": data,
      "lastId": 1
    });
  });

});
